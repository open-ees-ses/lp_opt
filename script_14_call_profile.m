%% Script Description
%	Coded and provided by:
%	Stefan Englberger (stefan.englberger@tum.de)
%	Team Stationary Energy Storage Systems (SES)
%	Institute for Electrical Energy Storage Technology
%	TUM Department of Electrical and Computer Engineering
%	Technical University of Munich
%
% 2019-07-11 Stefan Englberger (author)
%
% Englberger, S., Hesse, H., Kucevic, D., & Jossen, A. (2019). A Techno-Economic Analysis 
% of Vehicle-to-Building: % Battery Degradation and Efficiency Analysis in the Context of 
% Coordinated Electric Vehicle Charging. Energies, 12(5), 955.

%% Call generation profile
if Input.simulation.ModuleGeneration == double(true)
	load([Input.settings.FolderProfiles,'input'],'profile_pv');

	PowerGeneration							= double(profile_pv);
	x										= transpose(floor(linspace(1,length(PowerGeneration),length(PowerGeneration)/(Input.simulation.SampleTime/60))));
	PowerGeneration							= interp1(1:length(PowerGeneration),PowerGeneration,x);

	PowerGeneration							= PowerGeneration/max(PowerGeneration);
	PowerGeneration							= PowerGeneration*Input.generation.PeakPower;

	PowerGeneration							= repmat(PowerGeneration,ceil(Input.simulation.SimEnd/(60*60*24*365)),1);
	Input.generation.PowerGeneration		= PowerGeneration(Input.simulation.SimStart/Input.simulation.SampleTime+1:Input.simulation.SimEnd/Input.simulation.SampleTime,:);
	Input.generation.PowerGeneration		= Input.generation.PowerGeneration.*(1-Input.generation.Degradation);
else
	Input.generation.PowerGeneration		= Input.simulation.Samples * 0;
end
	Input.generation.PowerGeneration		= circshift(Input.generation.PowerGeneration,(60*60*24*5)/Input.simulation.SampleTime);

%% Call load profile
	load([Input.settings.FolderProfiles,'input'],'profile_load_hh');
dummy = 1;
for profile = Input.load.ProfileNo
	PowerLoad								= double(profile_load_hh);
	x										= transpose(floor(linspace(1,length(PowerLoad),length(PowerLoad)/(Input.simulation.SampleTime/60))));
	PowerLoad								= interp1(1:length(PowerLoad),PowerLoad,x);

	PowerLoad								= PowerLoad/sum(PowerLoad);
	PowerLoad								= PowerLoad*Input.load.AnnualEnergyDemand(dummy)/Input.simulation.SampleTime;

	PowerLoad								= repmat(PowerLoad,ceil(Input.simulation.SimEnd/(60*60*24*365)),1);
	PowerLoad								= PowerLoad(Input.simulation.SimStart/Input.simulation.SampleTime+1:Input.simulation.SimEnd/Input.simulation.SampleTime,:);

	Input.load.PowerLoad(:,dummy)			= PowerLoad(:);
	dummy = dummy+1;
end
	Input.load.PowerLoad					= circshift(Input.load.PowerLoad,(60*60*24*2)/Input.simulation.SampleTime);

%% Call electric vehicle profiles (demand & connection vector)
if Input.simulation.ModuleEVESS == double(true)
	load([Input.settings.FolderProfiles,'input'],'profile_driving_logic','profile_driving_power','profile_plugged_logic');
	for j = 1:Input.simulation.Entities
	% EV demand vector
		PowerEV									= double(profile_driving_power(:,Input.ev.ProfileNo(j)));
		x										= transpose(floor(linspace(1,length(PowerEV),length(PowerEV)/(Input.simulation.SampleTime/60))));
		PowerEV									= interp1(1:length(PowerEV),PowerEV,x);

		PowerEV									= PowerEV/sum(PowerEV);
		PowerEV									= PowerEV*Input.ev.AnnualEnergyDemand(j)/(365/7*Input.simulation.SampleTime);

		PowerEV									= repmat(PowerEV,ceil(Input.simulation.SimEnd/(60*60*24*7)),1);
		PowerEV									= PowerEV(Input.simulation.SimStart/Input.simulation.SampleTime+1:Input.simulation.SimEnd/Input.simulation.SampleTime,:);

		Input.ev.PowerEVLoad(:,j)				= PowerEV;
		Input.ev.PowerEVLoad(Input.ev.PowerEVLoad<0) = 0;
		Input.ev.PowerEVRecuperation(:,j)		= PowerEV;
		Input.ev.PowerEVRecuperation(Input.ev.PowerEVRecuperation>0) = 0;

	% EV plug in vector
		PluggedEV								= double(profile_plugged_logic(:,Input.ev.ProfileNo(j)));
		x										= transpose(floor(linspace(1,length(PluggedEV),length(PluggedEV)/(Input.simulation.SampleTime/60))));
		PluggedEV								= interp1(1:length(PluggedEV),PluggedEV,x);

		PluggedEV								= repmat(PluggedEV,ceil(Input.simulation.SimEnd/(60*60*24*7)),1);
		Input.chargingstation.PluggedEV(:,j)	= PluggedEV(Input.simulation.SimStart/Input.simulation.SampleTime+1:Input.simulation.SimEnd/Input.simulation.SampleTime,:);
	end
else
	Input.ev.PowerEVLoad						= repmat(Input.simulation.Samples * 0,1,Input.simulation.Entities);
	Input.ev.PowerEVRecuperation				= repmat(Input.simulation.Samples * 0,1,Input.simulation.Entities);
	Input.chargingstation.PluggedEV				= repmat(Input.simulation.Samples * 0,1,Input.simulation.Entities);
end