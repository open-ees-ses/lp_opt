%% Script Description
%	Coded and provided by:
%	Stefan Englberger (stefan.englberger@tum.de)
%	Team Stationary Energy Storage Systems (SES)
%	Institute for Electrical Energy Storage Technology
%	TUM Department of Electrical and Computer Engineering
%	Technical University of Munich
%
% 2019-07-11 Stefan Englberger (author)
%
% Englberger, S., Hesse, H., Kucevic, D., & Jossen, A. (2019). A Techno-Economic Analysis 
% of Vehicle-to-Building: % Battery Degradation and Efficiency Analysis in the Context of 
% Coordinated Electric Vehicle Charging. Energies, 12(5), 955.

%% Simulation (#sim)
%Single entities are always operating autonomously
	if Input.simulation.Entities == 1 && Input.simulation.EnergySharing == 1
		Input.simulation.EnergySharing		= double(false);
		warning('Single entities are always operating autonomously.');
	end
%Array of sample
	Input.simulation.Samples				= transpose(1:1:(Input.simulation.SimEnd-Input.simulation.SimStart)/Input.simulation.SampleTime);

%% Economics (#economics)
%Compound growth rate
	Input.economics.CompoundGrowthRate		= (1+Input.economics.AnnualGrowthRate/100)^(1/(3600*24*365/Input.simulation.SampleTime))-1;
	Input.economics.CompoundGrowth			= ones(length(Input.simulation.Samples),1);
	for i = 2:length(Input.economics.CompoundGrowth)
		Input.economics.CompoundGrowth(i)	= Input.economics.CompoundGrowth(i-1)*(1+Input.economics.CompoundGrowthRate);
	end

%% Generation (#generation)
	Input.generation.Degradation			= Input.generation.DegradationRate/(60*60*24*365)*Input.simulation.SampleTime.*Input.simulation.Samples;

	Input.simulation.AC_Generation			= 1-Input.simulation.DC_SESS;

%% Load (#load)

%% Stationary Electrical Energy Storage (#sess)
%Nominal energy content of SESS
if Input.simulation.ModuleSESS == double(true)
	Input.sess.CalendarDegradation			= Input.sess.CalendarDegradationRate.*(Input.simulation.Samples.^0.5/(Input.simulation.Samples(end))^0.5);
	Input.sess.CycleDegradation				= Input.sess.CycleDegradationRate.*(Input.simulation.Samples.^0.95/(Input.simulation.Samples(end))^0.95);
else
	Input.sess.CalendarDegradation			= Input.simulation.Samples * 0;
	Input.sess.CycleDegradation				= Input.simulation.Samples * 0;
end
	Input.sess.EnergyAvailable				= Input.sess.EnergyNominal.*(1-min(1,Input.sess.CalendarDegradation+Input.sess.CycleDegradation));

	Input.sess.EnergySelfDischarge			= Input.sess.EnergyAvailable.*Input.sess.SelfDischarge * Input.simulation.SampleTime;

	Input.simulation.AC_SESS				= 1-Input.simulation.DC_Generation;

%% charging station (#chargingstation)
	Input.simulation.AC_CS					= 1-Input.simulation.DC_CS;

%% Electric Vehicle (#ev)
%Annual distance equivalent of EV [km]
	Input.ev.DistanceEquivalent				= Input.ev.AnnualEnergyDemand./(Input.ev.Consumption/100*3.6e6);

%% Electric Vehicle Electrical Energy Storage (#evess)
if Input.simulation.ModuleEVESS == double(true)
	Input.evess.CalendarDegradation			= Input.evess.CalendarDegradationRate.*(Input.simulation.Samples.^0.5/(Input.simulation.Samples(end))^0.5);
	Input.evess.CycleDegradation			= Input.evess.CycleDegradationRate.*(Input.simulation.Samples.^0.95/(Input.simulation.Samples(end))^0.95);
else
	Input.evess.CalendarDegradation			= Input.simulation.Samples * 0;
	Input.evess.CycleDegradation			= Input.simulation.Samples * 0;
end
	Input.evess.EnergyAvailable				= Input.evess.EnergyNominal.*(1-min(1,Input.evess.CalendarDegradation+Input.evess.CycleDegradation));

	Input.evess.EnergySelfDischarge			= Input.evess.EnergyAvailable.*Input.evess.SelfDischarge * Input.simulation.SampleTime;

%% Point of Common Coupling (#pcc)

%% Superordinate Grid (#grid)
%Electricity selling price array
%Compound growth rate
	Input.grid.SellingPriceCompoundGrowthRate		= (1+Input.grid.SellingPriceGrowthRate/100)^(1/(3600*24*365/Input.simulation.SampleTime))-1;
	Input.grid.SellingPriceGrowth			= ones(length(Input.simulation.Samples),1);
	for i = 2:length(Input.grid.SellingPriceGrowth)
		Input.grid.SellingPriceGrowth(i)	= Input.grid.SellingPriceGrowth(i-1)*(1+Input.grid.SellingPriceCompoundGrowthRate);
	end

	Input.grid.SellingPrice					= Input.grid.SellingPriceInitial*Input.grid.SellingPriceGrowth;

%Electricity purchasing price array
	Input.grid.PurchasingPriceCompoundGrowthRate		= (1+Input.grid.PurchasingPriceGrowthRate/100)^(1/(3600*24*365/Input.simulation.SampleTime))-1;
	Input.grid.PurchasingPriceGrowth			= ones(length(Input.simulation.Samples),1);
	for i = 2:length(Input.grid.PurchasingPriceGrowth)
		Input.grid.PurchasingPriceGrowth(i)	= Input.grid.PurchasingPriceGrowth(i-1)*(1+Input.grid.PurchasingPriceCompoundGrowthRate);
	end

	Input.grid.PurchasingPrice				= Input.grid.PurchasingPriceInitial*Input.grid.PurchasingPriceGrowth;

%% Internal Combustion Engine Vehicle (#icevehicle)
%Compound growth rate
	Input.icevehicle.CostsFuelCompoundGrowthRate		= (1+Input.icevehicle.CostsFuelGrowthRate/100)^(1/(3600*24*365/Input.simulation.SampleTime))-1;
	Input.icevehicle.CostsFuelGrowth			= ones(length(Input.simulation.Samples),1);
	for i = 2:length(Input.icevehicle.CostsFuelGrowth)
		Input.icevehicle.CostsFuelGrowth(i)	= Input.icevehicle.CostsFuelGrowth(i-1)*(1+Input.icevehicle.CostsFuelCompoundGrowthRate);
	end

	Input.icevehicle.CostsFuel				= Input.icevehicle.CostsFuelInitial*Input.icevehicle.CostsFuelGrowth;
	
% Driving Distance
	Input.icevehicle.DrivingDistance = (Input.ev.AnnualEnergyDemand / 3.6e6) / (Input.ev.Consumption / 100) * length(Input.simulation.Samples)*Input.simulation.SampleTime/(60*60*24*365);
% Total Fuel Consumptions in liters
	Input.icevehicle.TotalFuelLiter = Input.icevehicle.DrivingDistance * (Input.icevehicle.Consumption / 100);
% Profit from purchasng fuel
	Input.icevehicle.Profit = -(Input.icevehicle.TotalFuelLiter /  length(Input.simulation.Samples) .* ones(length(Input.simulation.Samples),1)) .* Input.icevehicle.CostsFuel;