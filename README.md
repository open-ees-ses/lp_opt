# Linear programming optimization tool for energy storage systems (lp_opt)
This stand-alone tool generates the optimized operating strategy for one or multiple households with home energy storage systems and electric vehicles. Due to its flexible structure, this framework enables the simultaneous optimization of multiple storage systems.

The tool was created by Stefan Englberger at the Institute for Electrical Energy Storage Technology at the Technical University of Munich.

### **Highlights & Features:**

#### Co-optimization of stationery and vehicle storage systems' operation strategy on residential and microgrid level
- Multi-storage optimization for residential power flow model
- Minimization of operating expenses in form of electricity costs
- Maximization of self-consumption and self-sufficiency
- Minimization of curtailment losses

#### Techno-economic evaluation
- Uni-directional and bi-directional charging schemes of electric vehicle
- Impact of optimized charging
- Considering varying vehicle usage patterns
- Added value of home energy storage system
- Comparison with internal combustion engine powered vehicle
- Degradation of stationery and vehicle storage system

#### Considering multiple components of residential entity
- Stationery home energy storage system
- Electric vehicle with battery storage system
- Photovoltaic generator
- Electricity consumption

### **How to cite:**

[Englberger, S., Hesse, H., Kucevic, D., & Jossen, A. (2019). A Techno-Economic Analysis of Vehicle-to-Building: Battery Degradation and Efficiency Analysis in the Context of Coordinated Electric Vehicle Charging. _Energies, 12_(5), 955.](https://www.mdpi.com/1996-1073/12/5/955)
