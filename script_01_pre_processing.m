%% Script Description
%	Coded and provided by:
%	Stefan Englberger (stefan.englberger@tum.de)
%	Team Stationary Energy Storage Systems (SES)
%	Institute for Electrical Energy Storage Technology
%	TUM Department of Electrical and Computer Engineering
%	Technical University of Munich
%
% 2019-07-11 Stefan Englberger (author)
%
% Englberger, S., Hesse, H., Kucevic, D., & Jossen, A. (2019). A Techno-Economic Analysis 
% of Vehicle-to-Building: % Battery Degradation and Efficiency Analysis in the Context of 
% Coordinated Electric Vehicle Charging. Energies, 12(5), 955.

%% Clean operation environment
	%Close all figures
	close all;
	%Clear command window
	clc;

%% Disable specific warnings
	warning off backtrace
	warning off verbose

%% Define default values
	set(0,	'defaultAxesFontName', 'Palatino Linotype', ...
			'DefaultAxesFontWeight', 'normal', ...
			'defaultAxesFontSize', 14);

%% Define model environment and directories
	addpath(genpath(fileparts(which('script_00_main.m'))));
	Input.settings.FolderProfiles = '';
	addpath(Input.settings.FolderProfiles);

%% Generate reproducable random values
	rng(1);

%% Define simulation index
	SimulationIndex = 7;