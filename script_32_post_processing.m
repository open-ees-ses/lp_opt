%% Script Description
%	Coded and provided by:
%	Stefan Englberger (stefan.englberger@tum.de)
%	Team Stationary Energy Storage Systems (SES)
%	Institute for Electrical Energy Storage Technology
%	TUM Department of Electrical and Computer Engineering
%	Technical University of Munich
%
% 2019-07-11 Stefan Englberger (author)
%
% Englberger, S., Hesse, H., Kucevic, D., & Jossen, A. (2019). A Techno-Economic Analysis 
% of Vehicle-to-Building: % Battery Degradation and Efficiency Analysis in the Context of 
% Coordinated Electric Vehicle Charging. Energies, 12(5), 955.

%% Clean operation environment
	%Clear workspace (keep only specific variables)
	clearvars -except Input Optimization;

%% Export data
	%Generate results folder within model environment
	if exist('Results','dir') ~= 7; mkdir 'Results'; end
	%Generate time step of current simulation date
	Input.simulation.SimDate = datestr(now,'yyyymmdd_HHMMSS');
	%Export and save information in mat-file
	save(['Results/',Input.simulation.SimDate,'_export.mat']);
	%Export and save plot as fig-file
	savefig(['Results/',Input.simulation.SimDate,'_figure.fig']);
	%Export and save plot as png-file
	saveas(gcf,['Results/',Input.simulation.SimDate,'_figure.png']);