%% Script Description
%	Coded and provided by:
%	Stefan Englberger (stefan.englberger@tum.de)
%	Team Stationary Energy Storage Systems (SES)
%	Institute for Electrical Energy Storage Technology
%	TUM Department of Electrical and Computer Engineering
%	Technical University of Munich
%
% 2019-07-11 Stefan Englberger (author)
%
% Englberger, S., Hesse, H., Kucevic, D., & Jossen, A. (2019). A Techno-Economic Analysis 
% of Vehicle-to-Building: % Battery Degradation and Efficiency Analysis in the Context of 
% Coordinated Electric Vehicle Charging. Energies, 12(5), 955.

%% Simulation (#simulation)
%Start of simulation time [s]
	Input.simulation.SimStart				= 60*60*24*150;
%End of simulation time [s]
	Input.simulation.SimEnd					= Input.simulation.SimStart+60*60*24*5*1;
%Sampling time [s]
	Input.simulation.SampleTime				= 60*15;
%Number of participating entities/players [1]
	Input.simulation.Entities				= 2;
%Electricity trading allowed [binary]
	Input.simulation.EnergySharing			= double(true);
%Generation module activated [binary]
	Input.simulation.ModuleGeneration		= double(true);
%SESS module activated [binary]
	Input.simulation.ModuleSESS				= double(true);
%EVESS module activated [binary]
	Input.simulation.ModuleEVESS			= double(true);
%Control variable for vehicle-to-building [binary]
	Input.simulation.V2B					= double(true * ones(1,Input.simulation.Entities));
%Control variable for simple charging [binary]
	Input.simulation.SimpleCharge			= double(false * ones(1,Input.simulation.Entities));
%Control variable for AC & DC power flow to/from #generation [binary]
	Input.simulation.DC_Generation			= double(false * ones(1,Input.simulation.Entities));
%Control variable for AC & DC power flow to/from #evess [binary]
	Input.simulation.DC_SESS				= double(false * ones(1,Input.simulation.Entities));
%Control variable for AC & DC power flow to/from #chargingstation [binary]
	Input.simulation.DC_CS					= double(false * ones(1,Input.simulation.Entities));

%% Busbar (#busbar)
%Maximum cable power within building [W]
	Input.busbar.MaxPower					= 400*16*sqrt(3) * ones(1,Input.simulation.Entities);
%Efficiency of AC-DC converter between AC busbar and DC busbar [1]
	Input.busbar.Eta_ACDC					= 0.95 * ones(1,Input.simulation.Entities);

%% Generation (#generation)
%Filename of data source for profile data
	Input.generation.Profile = 'PV_EEN_Power_Munich_2014.mat';
%Type of power generator [nu]
	Input.generation.Type					= cell(1,Input.simulation.Entities);
	Input.generation.Type(:)				= {'PV'};
%Maximum power of power generator [W]
	Input.generation.PeakPower				= 5.0e3 * ones(1,Input.simulation.Entities);
%Annual degradation of power generator [1]
	Input.generation.DegradationRate		= 0.005 * ones(1,Input.simulation.Entities);
%Efficiency of DC-DC converter at generator [1]
	Input.generation.Eta_DCDC				= 0.97 * ones(1,Input.simulation.Entities);
%Efficiency of AC-DC converter at generator [1]
	Input.generation.Eta_ACDC				= 0.95 * ones(1,Input.simulation.Entities);

%% Load (#load)
%Filename of data source for profile data
	Input.load.Profile = 'Loadprofile_HTW_Familiy_2010.mat';
%Number of load profile
	Input.load.ProfileNo					= 31:1:31+Input.simulation.Entities-1;
%Annual electricity demand [Ws]
	Input.load.AnnualEnergyDemand			= 4000 * 3.6e6 * ones(1,Input.simulation.Entities);

%% Stationary Electrical Energy Storage (#sess)
%Type of battery chemistry [nu]
	Input.sess.Type							= cell(1,Input.simulation.Entities);
	Input.sess.Type(:)						= {'LFP'};
%Nominal energy content of SESS [Ws]
	Input.sess.EnergyNominal				= 4.0 * 3.6e6*ones(1,Input.simulation.Entities);
%Self-discharge of SESS per second [s^-1]
	Input.sess.SelfDischarge				= mean([0.02,0.1])/(30.5*24*3600) * ones(1,Input.simulation.Entities);
%End of Life Criterion of SESS [1]
	Input.sess.EoLCriterion					= 0.8 * ones(1,Input.simulation.Entities);
%Calendar degradation of SESS [1]
	Input.sess.CalendarDegradationRate		= 0.0450 * ones(1,Input.simulation.Entities);
%Cycle degradation of SESS [1]
	Input.sess.CycleDegradationRate			= 0.0095 * ones(1,Input.simulation.Entities);
%Minimum SOC of SESS [1]
	Input.sess.SOC_min						= 0.1 * ones(1,Input.simulation.Entities);
%Maximum SOC of SESS [1]
	Input.sess.SOC_max						= 0.9 * ones(1,Input.simulation.Entities);
%Initial SOC of SESS at the simulation start [1]
	Input.sess.SOC_initial					= 0.5 * ones(1,Input.simulation.Entities);
%End SOC of SESS at the simulation end [1]
	Input.sess.SOC_end						= 0.5 * ones(1,Input.simulation.Entities);
%C-Rate during SESS charge [h^-1]
	Input.sess.CRate_in						= 0.5 * ones(1,Input.simulation.Entities);
%C-Rate during SESS discharge [h^-1]
	Input.sess.CRate_out					= 0.5 * ones(1,Input.simulation.Entities);
%Efficiency during SESS charge [1]
	Input.sess.Eta_EES_in					= 0.99 * ones(1,Input.simulation.Entities);
%Efficiency during SESS discharge [1]
	Input.sess.Eta_EES_out					= 0.99 * ones(1,Input.simulation.Entities);
%Efficiency of DC-DC converter at SESS [1]
	Input.sess.Eta_DCDC						= 0.97 * ones(1,Input.simulation.Entities);
%Efficiency of AC-DC converter at SESS [1]
	Input.sess.Eta_ACDC						= 0.95 * ones(1,Input.simulation.Entities);

%% Charging station (#chargingstation)
%Efficiency of DC-DC converter at CS [1]
	Input.chargingstation.Eta_DCDC			= 0.97 * ones(1,Input.simulation.Entities);
%Efficiency of AC-DC converter at CS [1]
	Input.chargingstation.Eta_ACDC			= 0.95 * ones(1,Input.simulation.Entities);
%Maximum power of EV charging box [W]
	Input.chargingstation.MaxPower			= 11e3 * ones(1,Input.simulation.Entities);

%% Electric Vehicle (#ev)
%Filename of data source for profile data
Input.ev.Profile = 'Electric_Vehicle_1week_1min.mat';
%Number of EV profile
	Input.ev.ProfileNo						= randi([1,2],1,Input.simulation.Entities);
%Annual electricity demand [Ws]
	Input.ev.AnnualEnergyDemand				= 3000 * 3.6e6 * ones(1,Input.simulation.Entities);
%Electricity consumption of EV [kWH/100km]
	Input.ev.Consumption					= 12.9 * ones(1,Input.simulation.Entities);
%Investment costs of EV [EUR]
	Input.ev.CostsInvest					= 35e3 * ones(1,Input.simulation.Entities);
%Annual depreciation of EV (straight-line method) [1]
	Input.ev.AnnualDepreciation				= 0.1 * ones(1,Input.simulation.Entities);

%% Electric Vehicle Electrical Energy Storage (#evess)
%Type of battery chemistry [nu]
	Input.evess.Type						= cell(1,Input.simulation.Entities);
	Input.evess.Type(:)						= {'NMC'};
%Nominal energy content of EVESS [Ws]
	Input.evess.EnergyNominal				= 21.6 * 3.6e6*ones(1,Input.simulation.Entities);
%Self-discharge of EVESS per second [s^-1]
	Input.evess.SelfDischarge				= mean([0.02,0.1])/(30.5*24*3600) * ones(1,Input.simulation.Entities);
%End of Life Criterion of EVESS [1]
	Input.evess.EoLCriterion				= 0.8 * ones(1,Input.simulation.Entities);
%Calendar degradation of EVESS [1]
	Input.evess.CalendarDegradationRate		= 0.037 * ones(1,Input.simulation.Entities);
%Cycle degradation of EVESS [1]
	Input.evess.CycleDegradationRate		= 0.019 * ones(1,Input.simulation.Entities);
%Minimum SOC of EVESS [1]
	Input.evess.SOC_min						= 0.08 * ones(1,Input.simulation.Entities);
%Maximum SOC of EVESS [1]
	Input.evess.SOC_max						= 0.95 * ones(1,Input.simulation.Entities);
%Initial SOC of EVESS at the simulation start [1]
	Input.evess.SOC_initial					= 0.5 * ones(1,Input.simulation.Entities);
%End SOC of EVESS at the simulation end [1]
	Input.evess.SOC_end						= 0.5 * ones(1,Input.simulation.Entities);
%C-Rate during EVESS charge [h^-1]
	Input.evess.CRate_in					= 2.0 * ones(1,Input.simulation.Entities);
%C-Rate during EVESS discharge [h^-1]
	Input.evess.CRate_out					= 2.0 * ones(1,Input.simulation.Entities);
%Efficiency during EVESS charge [1]
	Input.evess.Eta_EES_in					= 0.97 * ones(1,Input.simulation.Entities);
%Efficiency during EVESS discharge [1]
	Input.evess.Eta_EES_out					= 0.97 * ones(1,Input.simulation.Entities);
%Efficiency of AC-DC converter at EVESS [1]
	Input.evess.Eta_ACDC					= 0.95 * ones(1,Input.simulation.Entities);

%% Point of Common Coupling (#pcc)
%Maximum line power between busbar and PCC [W]
	Input.pcc.MaxLinePower					= 19.2e3 * ones(1,Input.simulation.Entities);

%% Superordinate Grid (#grid)
%Electricity selling price [EUR/Ws]
	Input.grid.SellingPriceInitial			= 0.12/(3.6e6);
%Growth rate of selling price
	Input.grid.SellingPriceGrowthRate		= 0.0;
%Electricity purchasing price [EUR/Ws]
	Input.grid.PurchasingPriceInitial		= 0.30/(3.6e6);
%Growth rate of purchasing price
	Input.grid.PurchasingPriceGrowthRate	= 3.35;
%Regulated curtailment limit [1]
	Input.grid.Curtailment_Limit			= 0.5 * ones(1,Input.simulation.Entities);

%% Internal Combustion Engine Vehicle (#icevehicle)
%Electricity consumption of ICE vehicle [liter/100km]
	Input.icevehicle.Consumption			= 5.3 * ones(1,Input.simulation.Entities);
%Investment costs of ICE vehicle [EUR]
	Input.icevehicle.CostsInvest			= 25e3 * ones(1,Input.simulation.Entities);
%Annual depreciation of ICE vehicle (straight-line method) [1]
	Input.icevehicle.AnnualDepreciation		= 0.1 * ones(1,Input.simulation.Entities);
%Growth rate of fuel costs
	Input.icevehicle.CostsFuelGrowthRate	= 2.25;
%Fuel costs [EUR/liter]
	Input.icevehicle.CostsFuelInitial		= 1.45;

%% Economics (#economics)
%Annual macro-economic growth rate [%]
	Input.economics.AnnualGrowthRate		= 2.0;
%Annual interest rate [%]
	Input.economics.AnnualInterestRate		= 1.5;