%% Script Description
%	Coded and provided by:
%	Stefan Englberger (stefan.englberger@tum.de)
%	Team Stationary Energy Storage Systems (SES)
%	Institute for Electrical Energy Storage Technology
%	TUM Department of Electrical and Computer Engineering
%	Technical University of Munich
%
% 2019-07-11 Stefan Englberger (author)
%
% Englberger, S., Hesse, H., Kucevic, D., & Jossen, A. (2019). A Techno-Economic Analysis 
% of Vehicle-to-Building: % Battery Degradation and Efficiency Analysis in the Context of 
% Coordinated Electric Vehicle Charging. Energies, 12(5), 955.

%% PreAllocations
SimTime = Input.simulation.SimEnd-Input.simulation.SimStart;
switch true
	case SimTime>60*60*24*365*600
		TimeUnit = 'years';
		AxisTime = Input.simulation.Samples*Input.simulation.SampleTime/(60*60*24*365);
	case SimTime>60*60*24*3
		TimeUnit = 'days';
		AxisTime = Input.simulation.Samples*Input.simulation.SampleTime/(60*60*24);
	case SimTime>60*60*3
		TimeUnit = 'hours';
		AxisTime = Input.simulation.Samples*Input.simulation.SampleTime/(60*60);
	case SimTime<=60*60*3
		TimeUnit = 'seconds';
		AxisTime = Input.simulation.Samples*Input.simulation.SampleTime;
	otherwise 
		warning('Chosen time unit not available.');
end
AxisTime = transpose(AxisTime);

%% Preallocate plotting variables
	P_entity		= Optimization.DecisionVariables.P_ac_bbTpcc-Optimization.DecisionVariables.P_ac_bbFpcc;
	if isempty(P_entity); P_entity = zeros(length(Input.simulation.Samples),Input.simulation.Entities); end
	P_generation	= -(Optimization.DecisionVariables.P_ac_bbFgen+Optimization.DecisionVariables.P_dc_bbFgen);
	if isempty(P_generation); P_generation = zeros(length(Input.simulation.Samples),Input.simulation.Entities); end
	P_load			= Input.load.PowerLoad;
	if isempty(P_load); P_load = zeros(length(Input.simulation.Samples),Input.simulation.Entities); end
	P_evload		= Optimization.DecisionVariables.P_ac_bbTevess+Optimization.DecisionVariables.P_dc_bbTevess;
	if isempty(P_evload); P_evload = zeros(length(Input.simulation.Samples),Input.simulation.Entities); end
	P_share			= -(P_entity-mean(P_entity,2));
	if isempty(P_share); P_share = zeros(length(Input.simulation.Samples),Input.simulation.Entities); end
	P_trade			= -(P_entity-P_share);
	if isempty(P_trade); P_trade = zeros(length(Input.simulation.Samples),Input.simulation.Entities); end
	P_sess			= Optimization.DecisionVariables.P_ac_bbFsess+Optimization.DecisionVariables.P_dc_bbFsess-Optimization.DecisionVariables.P_ac_bbTsess-Optimization.DecisionVariables.P_dc_bbTsess;
	if isempty(P_sess); P_sess = zeros(length(Input.simulation.Samples),Input.simulation.Entities); end
	P_evess			= Optimization.DecisionVariables.P_ac_bbFevess+Optimization.DecisionVariables.P_dc_bbFevess-Optimization.DecisionVariables.P_ac_bbTevess-Optimization.DecisionVariables.P_dc_bbTevess;
	if isempty(P_evess); P_evess = zeros(length(Input.simulation.Samples),Input.simulation.Entities); end
	P_v2b			= -(Optimization.DecisionVariables.P_ac_bbFevess+Optimization.DecisionVariables.P_dc_bbFevess);
	if isempty(P_v2b); P_v2b = zeros(length(Input.simulation.Samples),Input.simulation.Entities); end
	if isfield(Optimization.DecisionVariables,'P_curtailment') == 0; P_curtailment = zeros(size(P_generation)); else; P_curtailment = -Optimization.DecisionVariables.P_curtailment; end
	if isempty(P_curtailment); P_curtailment = zeros(length(Input.simulation.Samples),Input.simulation.Entities); end
	P_residual		= P_load+P_evload+P_generation;

	rgbMatrix = [0.9  0.9    0;		% Generation Power
				 0.4  0.4  0.4;		% Load Power
				 0.6  0.6  0.6;		% Load Power (EV)
				   0  0.6    0;		% Traded Power
				   0  0.9    0;		% Shared Power
				   0    0  0.9;		% Storage Power (SESS)
				 0.5  0.5  1.0;		% Storage Power (EVESS)
				 0.8  0.4  0.9;		% Power Vehicle to Building
				 0.8    0    0];	% Curtailment Power (Losses)
	alpha = [1.0  1.0  1.0  1.0  1.0  1.0  1.0  1.0  1.0];

	legstring = {'Generation', 'Load', 'Load (EV)', 'Trading', 'Sharing', 'SESS', 'EVESS', 'V2B', 'Curtailment', 'Residual Load'};

%% Create Plots

%Preallocate Arrays of Graphics Objects
fig	= gobjects(1,Input.simulation.Entities);
h	= gobjects(1,3*Input.simulation.Entities);

%Power flows
for i = 1:Input.simulation.Entities
	fig(i) = figure('name',['Entity ',num2str(i)],'units','normalized','outerposition',[0 0 1 1]);

	h(i+0*Input.simulation.Entities) = subplot(2,2,[1 2]);
		title(['Power Streams @ Entity ',num2str(i)]);
		hold on;
		yyaxis left
		xlabel(['Time / ',TimeUnit]); xlim([0,AxisTime(end)]);
		ylabel('Power / W');
			p(1)	=	area(AxisTime,P_generation(:,i), 'EdgeColor', 'none');
			p(2:3)	=	area(AxisTime,[P_load(:,i),P_evload(:,i)], 'EdgeColor', 'none');
			p(4:9)	=	area(AxisTime,[P_trade(:,i), P_share(:,i), P_sess(:,i), P_evess(:,i), P_v2b(:,i), P_curtailment(:,i)], 'EdgeColor', 'none');
			for ii=1:9; p(ii).FaceColor = rgbMatrix(ii,:); p(ii).FaceAlpha = alpha(ii); end
% 			p(10)	=	plot(AxisTime,P_residual, 'LineWidth',0.8, 'Color',[0 0 0 0.5]);
		yyaxis right
		plot(AxisTime,cumsum(Input.grid.SellingPrice.*Optimization.DecisionVariables.P_ac_pccTgrid*Input.simulation.SampleTime-Input.grid.PurchasingPrice.*Optimization.DecisionVariables.P_ac_pccFgrid*Input.simulation.SampleTime),'DisplayName','Cumulative Profit');
		ylabel('Cumulative Profit / EUR');
		l = legend(p(1:end),'Location','south','Orientation','horizontal'); grid on; box on;
		l.String = legstring;
		hold off;
	
	if Input.simulation.ModuleSESS == 1
	h(i+1*Input.simulation.Entities) = subplot(2,2,3);
		title(['SOC and SOH of SESS (EFC: ',num2str(round(sum(Optimization.DecisionVariables.EFC_sess(:,i)),1)),')']);
		hold on;
		xlabel(['Time / ',TimeUnit]); xlim([0,AxisTime(end)]);
		yyaxis left
		ylabel('State of Charge / %'); ylim([0,100.0]);
		plot(AxisTime,Optimization.DecisionVariables.SOC_sess(:,i)*100.0,'DisplayName','SOC_{SESS}');
		yyaxis right
		ylabel('State of Health / %');
		plot(AxisTime,Input.sess.EnergyAvailable(:,i)./Input.sess.EnergyNominal(:,i)*100.0,'DisplayName','SOH_{SESS}');
		legend off; grid on; box on;
		hold off;
	end

	if Input.simulation.ModuleEVESS == 1
	h(i+2*Input.simulation.Entities) = subplot(2,2,4);
		title(['SOC and SOH of EVESS (EFC: ',num2str(round(sum(Optimization.DecisionVariables.EFC_evess(:,i)),1)),')']);
		hold on;
		xlabel(['Time / ',TimeUnit]); xlim([0,AxisTime(end)]);
		yyaxis left
		ylabel('State of Charge / %'); ylim([0,100.0]);
		plot(AxisTime,Optimization.DecisionVariables.SOC_evess(:,i)*100.0,'DisplayName','SOC_{EVESS}');
		yyaxis right
		ylabel('State of Health / %');
		plot(AxisTime,Input.evess.EnergyAvailable(:,i)./Input.evess.EnergyNominal(:,i)*100.0,'DisplayName','SOH_{EVESS}');
		legend off; grid on; box on;
		hold off;
	end
end

%% Link axis
linkaxes(h(:),'x');