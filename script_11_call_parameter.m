%% Script Description
%	Coded and provided by:
%	Stefan Englberger (stefan.englberger@tum.de)
%	Team Stationary Energy Storage Systems (SES)
%	Institute for Electrical Energy Storage Technology
%	TUM Department of Electrical and Computer Engineering
%	Technical University of Munich
%
% 2019-07-11 Stefan Englberger (author)
%
% Englberger, S., Hesse, H., Kucevic, D., & Jossen, A. (2019). A Techno-Economic Analysis 
% of Vehicle-to-Building: % Battery Degradation and Efficiency Analysis in the Context of 
% Coordinated Electric Vehicle Charging. Energies, 12(5), 955.

%% Call Excel File
[num,txt,raw] = xlsread('Input','Input','C3:P203');

%% Convert data
	Input.simulation.Number		=	num(1,SimulationIndex);	% [nu], Number of simulation
	Input.simulation.Name		=	raw{2,SimulationIndex};	% [string], Name of simulation
	Input.simulation.SimStart		=	num(3,SimulationIndex);	% [s], Start of simulation time in seconds
	Input.simulation.SimEnd		=	num(4,SimulationIndex);	% [s], End of simulation time in seconds
	Input.simulation.SampleTime		=	num(5,SimulationIndex);	% [s], Sampling time in seconds
	Input.simulation.Entities		=	num(6,SimulationIndex);	% [nu], Number of participating entities/players
	Input.simulation.EnergySharing		=	num(7,SimulationIndex);	% [logical], Electricity trading allowed (1=enabled)
	Input.simulation.ModuleGeneration		=	num(8,SimulationIndex);	% [logical], Generation module activated (1=enabled)
	Input.simulation.ModuleSESS		=	num(9,SimulationIndex);	% [logical], SESS module activated (1=enabled)
	Input.simulation.ModuleEVESS		=	num(10,SimulationIndex);	% [logical], EVESS module activated (1=enabled)
	Input.simulation.V2B		=	num(11,SimulationIndex);	% [logical], Control variable for vehicle-to-building (1=enabled)
	Input.simulation.SimpleCharge		=	num(12,SimulationIndex);	% [logical], Control variable for simple charging of EV (1=enabled)
	Input.simulation.DC_Generation		=	num(13,SimulationIndex);	% [logical], Control variable for AC & DC power flow to/from Generation (1=enabled)
	Input.simulation.DC_SESS		=	num(14,SimulationIndex);	% [logical], Control variable for AC & DC power flow to/from SESS (1=enabled)
	Input.simulation.DC_CS		=	num(15,SimulationIndex);	% [logical], Control variable for AC & DC power flow to/from EVESS (1=enabled)
	Input.busbar.MaxPower		=	num(16,SimulationIndex);	% [W], Maximum cable power within building in watts
	Input.busbar.Eta_ACDC		=	num(17,SimulationIndex);	% [nu], Efficiency of AC-DC converter between AC busbar and DC busbar
	Input.generation.Type		=	raw{18,SimulationIndex};	% [string], Type of power generator
	Input.generation.Profile		=	raw{19,SimulationIndex};	% [string], Filename of data source for profile data
	Input.generation.PeakPower		=	num(20,SimulationIndex);	% [W], Maximum power of power generator
	Input.generation.DegradationRate		=	num(21,SimulationIndex);	% [nu], Annual degradation of power generator
	Input.generation.Eta_DCDC		=	num(22,SimulationIndex);	% [nu], Efficiency of DC-DC converter at generator
	Input.generation.Eta_ACDC		=	num(23,SimulationIndex);	% [nu], Efficiency of AC-DC converter at generator
	Input.load.Profile		=	raw{24,SimulationIndex};	% [string], Filename of data source for profile data
	Input.load.ProfileNo		=	num(25,SimulationIndex);	% [nu], Number of load profile
	Input.load.AnnualEnergyDemand		=	num(26,SimulationIndex);	% [Ws], Annual electricity demand in watt-seconds
	Input.sess.Type		=	raw{27,SimulationIndex};	% [string], Type of battery chemistry
	Input.sess.EnergyNominal		=	num(28,SimulationIndex);	% [Ws], Nominal energy content of SESS in watt-seconds
	Input.sess.SelfDischarge		=	num(29,SimulationIndex);	% [1/s], Self-discharge of SESS per second
	Input.sess.SOH_initial		=	num(30,SimulationIndex);	% [nu], Initial state of health
	Input.sess.EoLCriterion		=	num(31,SimulationIndex);	% [nu], End of Life Criterion of SESS
	Input.sess.CalendarDegradationRate		=	num(32,SimulationIndex);	% [nu], Calendar degradation of SESS
	Input.sess.CycleDegradationRate		=	num(33,SimulationIndex);	% [nu], Cycle degradation of SESS
	Input.sess.SOC_min		=	num(34,SimulationIndex);	% [nu], Minimum SOC of SESS
	Input.sess.SOC_max		=	num(35,SimulationIndex);	% [nu], Maximum SOC of SESS
	Input.sess.SOC_initial		=	num(36,SimulationIndex);	% [nu], Initial SOC of SESS at the simulation start
	Input.sess.SOC_end		=	num(37,SimulationIndex);	% [nu], End SOC of SESS at the simulation end
	Input.sess.CRate_in		=	num(38,SimulationIndex);	% [1/h], C-Rate during SESS charge
	Input.sess.CRate_out		=	num(39,SimulationIndex);	% [1/h], C-Rate during SESS discharge
	Input.sess.Eta_EES_in		=	num(40,SimulationIndex);	% [nu], Efficiency during SESS charge
	Input.sess.Eta_EES_out		=	num(41,SimulationIndex);	% [nu], Efficiency during SESS discharge
	Input.sess.RatedPowerPE		=	num(42,SimulationIndex);	% [W], Rated power of power electronics
	Input.sess.Eta_DCDC		=	num(43,SimulationIndex);	% [nu], Efficiency of DC-DC converter at SESS
	Input.sess.Eta_ACDC		=	num(44,SimulationIndex);	% [nu], Efficiency of AC-DC converter at SESS
	Input.chargingstation.RatedPowerPE		=	num(45,SimulationIndex);	% [W], Rated power of power electronics
	Input.chargingstation.Eta_DCDC		=	num(46,SimulationIndex);	% [nu], Efficiency of DC-DC converter at CS
	Input.chargingstation.Eta_ACDC		=	num(47,SimulationIndex);	% [nu], Efficiency of AC-DC converter at CS
	Input.chargingstation.MaxPower		=	num(48,SimulationIndex);	% [W], Maximum power of EV charging box in watts
	Input.ev.Type		=	raw{49,SimulationIndex};	% [string], Type of vehicle (classification)
	Input.ev.Profile		=	raw{50,SimulationIndex};	% [string], Filename of data source for profile data
	Input.ev.ProfileNo		=	num(51,SimulationIndex);	% [nu], Number of EV load profile
	Input.ev.AnnualEnergyDemand		=	num(52,SimulationIndex);	% [Ws], Annual electricity demand of EV in watt-seconds
	Input.ev.Consumption		=	num(53,SimulationIndex);	% [kWh/100km], Electricity consumption of EV
	Input.ev.CostsInvest		=	num(54,SimulationIndex);	% [EUR], Investment costs of EV
	Input.ev.AnnualDepreciation		=	num(55,SimulationIndex);	% [nu], Annual depreciation of EV (straight-line method)
	Input.evess.Type		=	raw{56,SimulationIndex};	% [string], Type of battery chemistry
	Input.evess.EnergyNominal		=	num(57,SimulationIndex);	% [Ws], Nominal energy content of EVESS in watt-seconds
	Input.evess.SelfDischarge		=	num(58,SimulationIndex);	% [1/s], Self-discharge of EVESS per second
	Input.sess.SOH_initial		=	num(59,SimulationIndex);	% [nu], Initial state of health
	Input.evess.EoLCriterion		=	num(60,SimulationIndex);	% [nu], End of Life Criterion of EVESS
	Input.evess.CalendarDegradationRate		=	num(61,SimulationIndex);	% [nu], Calendar degradation of EVESS
	Input.evess.CycleDegradationRate		=	num(62,SimulationIndex);	% [nu], Cycle degradation of EVESS
	Input.evess.SOC_min		=	num(63,SimulationIndex);	% [nu], Minimum SOC of EVESS
	Input.evess.SOC_max		=	num(64,SimulationIndex);	% [nu], Maximum SOC of EVESS
	Input.evess.SOC_initial		=	num(65,SimulationIndex);	% [nu], Initial SOC of EVESS at the simulation start
	Input.evess.SOC_end		=	num(66,SimulationIndex);	% [nu], End SOC of EVESS at the simulation end
	Input.evess.CRate_in		=	num(67,SimulationIndex);	% [1/h], C-Rate during EVESS charge
	Input.evess.CRate_out		=	num(68,SimulationIndex);	% [1/h], C-Rate during EVESS discharge
	Input.evess.Eta_EES_in		=	num(69,SimulationIndex);	% [nu], Efficiency during EVESS charge
	Input.evess.Eta_EES_out		=	num(70,SimulationIndex);	% [nu], Efficiency during EVESS discharge
	Input.evess.RatedPowerPE		=	num(71,SimulationIndex);	% [W], Rated power of power electronics
	Input.evess.Eta_ACDC		=	num(72,SimulationIndex);	% [nu], Efficiency of AC-DC converter at EVESS
	Input.pcc.MaxLinePower		=	num(73,SimulationIndex);	% [W], Maximum line power between busbar and PCC in watts
	Input.grid.SellingPriceInitial		=	num(74,SimulationIndex);	% [EUR/Ws], Electricity selling price
	Input.grid.SellingPriceGrowthRate		=	num(75,SimulationIndex);	% [%], Compound annual growth rate of electrcity selling price
	Input.grid.PurchasingPriceInitial		=	num(76,SimulationIndex);	% [EUR/Ws], Electricity purchasing price
	Input.grid.PurchasingPriceGrowthRate		=	num(77,SimulationIndex);	% [%], Compound annual growth rate of electricity purchasing price
	Input.grid.Curtailment_Limit		=	num(78,SimulationIndex);	% [nu], Regulated curtailment limit
	Input.icevehicle.Type		=	raw{79,SimulationIndex};	% [string], Type of vehicle (classification)
	Input.icevehicle.Consumption		=	num(80,SimulationIndex);	% [liter/100km], Consumption of ICE vehicle
	Input.icevehicle.CostsInvest		=	num(81,SimulationIndex);	% [EUR], Investment costs of ICE vehicle
	Input.icevehicle.CostsFuelInitial		=	num(82,SimulationIndex);	% [EUR/liter], Initial fuel costs of ICE vehicle
	Input.icevehicle.CostsFuelGrowthRate		=	num(83,SimulationIndex);	% [%], Compound annual growth rate of fuel price
	Input.icevehicle.AnnualDepreciation		=	num(84,SimulationIndex);	% [nu], Annual depreciation of ICE vehicle (straight-line method)
	Input.economics.AnnualGrowthRate		=	num(85,SimulationIndex);	% [%], Annual macro-economic growth rate in percent (inflation)
	Input.economics.AnnualInterestRate		=	num(86,SimulationIndex);	% [%], Annual interest rate

%% Clean operation environment
	%Clear workspace (keep only specific variables)
% 	clearvars -except Input Optimization;