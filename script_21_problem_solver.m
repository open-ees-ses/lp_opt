%% Script Description
%	Coded and provided by:
%	Stefan Englberger (stefan.englberger@tum.de)
%	Team Stationary Energy Storage Systems (SES)
%	Institute for Electrical Energy Storage Technology
%	TUM Department of Electrical and Computer Engineering
%	Technical University of Munich
%
% 2019-07-11 Stefan Englberger (author)
%
% Englberger, S., Hesse, H., Kucevic, D., & Jossen, A. (2019). A Techno-Economic Analysis 
% of Vehicle-to-Building: % Battery Degradation and Efficiency Analysis in the Context of 
% Coordinated Electric Vehicle Charging. Energies, 12(5), 955.

%% Define & Call Solver
% add the path of the gurobi solver, if available
	if exist('gurobi','file') == 3
		gurobipath = fileparts(which('gurobi'));
		addpath([gurobipath(1:end-6),'examples\matlab']);
	else
		promptMessage = sprintf('The Gurobi solver was not found in the MATLAB environment.\nDo you want to continue anyway?');
		button = questdlg(promptMessage, 'Continue without Gurobi solver', 'Continue', 'Cancel', 'Continue');
		if strcmpi(button, 'Cancel')
			error('Simulation was aborted by user.');
		end
	end

%Set solver options
	OptOptions = optimoptions('linprog','Algorithm','dual-simplex','OptimalityTolerance',1e-8,'ConstraintTolerance',1e-3,'MaxTime',10000);
%Call solver
	[OptDecisionVariables,OptObjectiveValue,OptExitFlag,OptSettings] = solve(OptProb,'Options',OptOptions);

%% Save optimization problem
	Optimization.Problem				=	OptProb;
	Optimization.Options				=	OptOptions;
	Optimization.Settings				=	OptSettings;
	Optimization.DecisionVariables		=	OptDecisionVariables;
	Optimization.ObjectiveValue			=	OptObjectiveValue;
	Optimization.ExitFlag				=	OptExitFlag;