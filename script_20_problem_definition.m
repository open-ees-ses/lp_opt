%% Script Description
%	Coded and provided by:
%	Stefan Englberger (stefan.englberger@tum.de)
%	Team Stationary Energy Storage Systems (SES)
%	Institute for Electrical Energy Storage Technology
%	TUM Department of Electrical and Computer Engineering
%	Technical University of Munich
%
% 2019-07-11 Stefan Englberger (author)
%
% Englberger, S., Hesse, H., Kucevic, D., & Jossen, A. (2019). A Techno-Economic Analysis 
% of Vehicle-to-Building: % Battery Degradation and Efficiency Analysis in the Context of 
% Coordinated Electric Vehicle Charging. Energies, 12(5), 955.

%% Create Decision Variables
	E_sess			=	optimvar('E_sess',length(Input.simulation.Samples),Input.simulation.Entities,'LowerBound',0,'UpperBound',repmat(Input.sess.EnergyNominal,length(Input.simulation.Samples),1));
	E_evess			=	optimvar('E_evess',length(Input.simulation.Samples),Input.simulation.Entities,'LowerBound',0,'UpperBound',repmat(Input.evess.EnergyNominal,length(Input.simulation.Samples),1));
	E_sess_in		=	optimvar('E_sess_in',length(Input.simulation.Samples),Input.simulation.Entities,'LowerBound',0,'UpperBound',repmat(Input.sess.EnergyNominal.*Input.sess.CRate_in/(Input.simulation.SampleTime/(60*60)),length(Input.simulation.Samples),1));
	E_sess_out		=	optimvar('E_sess_out',length(Input.simulation.Samples),Input.simulation.Entities,'LowerBound',0,'UpperBound',repmat(Input.sess.EnergyNominal.*Input.sess.CRate_out/(Input.simulation.SampleTime/(60*60)),length(Input.simulation.Samples),1));
	E_evess_in		=	optimvar('E_evess_in',length(Input.simulation.Samples),Input.simulation.Entities,'LowerBound',0,'UpperBound',repmat(Input.evess.EnergyNominal.*Input.evess.CRate_in/(Input.simulation.SampleTime/(60*60)),length(Input.simulation.Samples),1));
	E_evess_out		=	optimvar('E_evess_out',length(Input.simulation.Samples),Input.simulation.Entities,'LowerBound',0,'UpperBound',repmat(Input.evess.EnergyNominal.*Input.evess.CRate_out/(Input.simulation.SampleTime/(60*60)),length(Input.simulation.Samples),1));

	P_ac_genTbb		=	optimvar('P_ac_genTbb',length(Input.simulation.Samples),Input.simulation.Entities,'LowerBound',0,'UpperBound',repmat(Input.busbar.MaxPower,length(Input.simulation.Samples),1));
	P_ac_bbFgen		=	optimvar('P_ac_bbFgen',length(Input.simulation.Samples),Input.simulation.Entities,'LowerBound',0,'UpperBound',repmat(Input.busbar.MaxPower,length(Input.simulation.Samples),1));
	P_ac_bbTsess	=	optimvar('P_ac_bbTsess',length(Input.simulation.Samples),Input.simulation.Entities,'LowerBound',0,'UpperBound',repmat(Input.busbar.MaxPower,length(Input.simulation.Samples),1));
	P_ac_bbFsess	=	optimvar('P_ac_bbFsess',length(Input.simulation.Samples),Input.simulation.Entities,'LowerBound',0,'UpperBound',repmat(Input.busbar.MaxPower,length(Input.simulation.Samples),1));
	P_ac_bbTevess	=	optimvar('P_ac_bbTevess',length(Input.simulation.Samples),Input.simulation.Entities,'LowerBound',0,'UpperBound',repmat(min(Input.busbar.MaxPower,Input.chargingstation.MaxPower),length(Input.simulation.Samples),1));
	P_ac_bbFevess	=	optimvar('P_ac_bbFevess',length(Input.simulation.Samples),Input.simulation.Entities,'LowerBound',0,'UpperBound',repmat(min(Input.busbar.MaxPower,Input.chargingstation.MaxPower),length(Input.simulation.Samples),1));
	P_ac_sessTbb	=	optimvar('P_ac_sessTbb',length(Input.simulation.Samples),Input.simulation.Entities,'LowerBound',0,'UpperBound',repmat(Input.busbar.MaxPower,length(Input.simulation.Samples),1));
	P_ac_sessFbb	=	optimvar('P_ac_sessFbb',length(Input.simulation.Samples),Input.simulation.Entities,'LowerBound',0,'UpperBound',repmat(Input.busbar.MaxPower,length(Input.simulation.Samples),1));
	P_ac_evessTbb	=	optimvar('P_ac_evessTbb',length(Input.simulation.Samples),Input.simulation.Entities,'LowerBound',0,'UpperBound',repmat(min(Input.busbar.MaxPower,Input.chargingstation.MaxPower),length(Input.simulation.Samples),1));
	P_ac_evessFbb	=	optimvar('P_ac_evessFbb',length(Input.simulation.Samples),Input.simulation.Entities,'LowerBound',0,'UpperBound',repmat(min(Input.busbar.MaxPower,Input.chargingstation.MaxPower),length(Input.simulation.Samples),1));

	P_acbbTdcbb		=	optimvar('P_acbbTdcbb',length(Input.simulation.Samples),Input.simulation.Entities,'LowerBound',0,'UpperBound',repmat(Input.busbar.MaxPower,length(Input.simulation.Samples),1)*3);
	P_acbbFdcbb		=	optimvar('P_acbbFdcbb',length(Input.simulation.Samples),Input.simulation.Entities,'LowerBound',0,'UpperBound',repmat(Input.busbar.MaxPower,length(Input.simulation.Samples),1)*3);
	P_dcbbTacbb		=	optimvar('P_dcbbTacbb',length(Input.simulation.Samples),Input.simulation.Entities,'LowerBound',0,'UpperBound',repmat(Input.busbar.MaxPower,length(Input.simulation.Samples),1)*3);
	P_dcbbFacbb		=	optimvar('P_dcbbFacbb',length(Input.simulation.Samples),Input.simulation.Entities,'LowerBound',0,'UpperBound',repmat(Input.busbar.MaxPower,length(Input.simulation.Samples),1)*3);

	P_dc_genTbb		=	optimvar('P_dc_genTbb',length(Input.simulation.Samples),Input.simulation.Entities,'LowerBound',0,'UpperBound',repmat(Input.busbar.MaxPower,length(Input.simulation.Samples),1));
	P_dc_bbFgen		=	optimvar('P_dc_bbFgen',length(Input.simulation.Samples),Input.simulation.Entities,'LowerBound',0,'UpperBound',repmat(Input.busbar.MaxPower,length(Input.simulation.Samples),1));
	P_dc_bbTsess	=	optimvar('P_dc_bbTsess',length(Input.simulation.Samples),Input.simulation.Entities,'LowerBound',0,'UpperBound',repmat(Input.busbar.MaxPower,length(Input.simulation.Samples),1));
	P_dc_bbFsess	=	optimvar('P_dc_bbFsess',length(Input.simulation.Samples),Input.simulation.Entities,'LowerBound',0,'UpperBound',repmat(Input.busbar.MaxPower,length(Input.simulation.Samples),1));
	P_dc_bbTevess	=	optimvar('P_dc_bbTevess',length(Input.simulation.Samples),Input.simulation.Entities,'LowerBound',0,'UpperBound',repmat(Input.busbar.MaxPower,length(Input.simulation.Samples),1));
	P_dc_bbFevess	=	optimvar('P_dc_bbFevess',length(Input.simulation.Samples),Input.simulation.Entities,'LowerBound',0,'UpperBound',repmat(Input.busbar.MaxPower,length(Input.simulation.Samples),1));
	P_dc_sessTbb	=	optimvar('P_dc_sessTbb',length(Input.simulation.Samples),Input.simulation.Entities,'LowerBound',0,'UpperBound',repmat(Input.busbar.MaxPower,length(Input.simulation.Samples),1));
	P_dc_sessFbb	=	optimvar('P_dc_sessFbb',length(Input.simulation.Samples),Input.simulation.Entities,'LowerBound',0,'UpperBound',repmat(Input.busbar.MaxPower,length(Input.simulation.Samples),1));
	P_dc_evessTbb	=	optimvar('P_dc_evessTbb',length(Input.simulation.Samples),Input.simulation.Entities,'LowerBound',0,'UpperBound',repmat(Input.busbar.MaxPower,length(Input.simulation.Samples),1));
	P_dc_evessFbb	=	optimvar('P_dc_evessFbb',length(Input.simulation.Samples),Input.simulation.Entities,'LowerBound',0,'UpperBound',repmat(Input.busbar.MaxPower,length(Input.simulation.Samples),1));

	P_evessTevdrive	=	optimvar('P_evessTevdrive',length(Input.simulation.Samples),Input.simulation.Entities,'LowerBound',Input.ev.PowerEVLoad./repmat(Input.evess.Eta_ACDC,length(Input.simulation.Samples),1),'UpperBound',Input.ev.PowerEVLoad./repmat(Input.evess.Eta_ACDC,length(Input.simulation.Samples),1));
	P_evessFevdrive	=	optimvar('P_evessFevdrive',length(Input.simulation.Samples),Input.simulation.Entities,'LowerBound',Input.ev.PowerEVRecuperation.*repmat(Input.evess.Eta_ACDC,length(Input.simulation.Samples),1),'UpperBound',Input.ev.PowerEVRecuperation.*repmat(Input.evess.Eta_ACDC,length(Input.simulation.Samples),1));

	P_curtailment	=	optimvar('P_curtailment',length(Input.simulation.Samples),Input.simulation.Entities,'LowerBound',0,'UpperBound',Inf);

	P_ac_bbTpcc		=	optimvar('P_ac_bbTpcc',length(Input.simulation.Samples),Input.simulation.Entities,'LowerBound',0,'UpperBound',repmat(Input.pcc.MaxLinePower,length(Input.simulation.Samples),1));
	P_ac_bbFpcc		=	optimvar('P_ac_bbFpcc',length(Input.simulation.Samples),Input.simulation.Entities,'LowerBound',0,'UpperBound',repmat(Input.pcc.MaxLinePower,length(Input.simulation.Samples),1));
	P_ac_pccTbb		=	optimvar('P_ac_pccTbb',length(Input.simulation.Samples),Input.simulation.Entities,'LowerBound',0,'UpperBound',repmat(Input.pcc.MaxLinePower,length(Input.simulation.Samples),1));
	P_ac_pccFbb		=	optimvar('P_ac_pccFbb',length(Input.simulation.Samples),Input.simulation.Entities,'LowerBound',0,'UpperBound',repmat(Input.pcc.MaxLinePower,length(Input.simulation.Samples),1));
	P_ac_pccTgrid	=	optimvar('P_ac_pccTgrid',length(Input.simulation.Samples),1,'LowerBound',0,'UpperBound',repmat(sum(Input.pcc.MaxLinePower),length(Input.simulation.Samples),1));
	P_ac_pccFgrid	=	optimvar('P_ac_pccFgrid',length(Input.simulation.Samples),1,'LowerBound',0,'UpperBound',repmat(sum(Input.pcc.MaxLinePower),length(Input.simulation.Samples),1));

	SOC_sess		=	optimvar('SOC_sess',length(Input.simulation.Samples),Input.simulation.Entities,'LowerBound',repmat(Input.sess.SOC_min,length(Input.simulation.Samples),1),'UpperBound',repmat(Input.sess.SOC_max,length(Input.simulation.Samples),1));
	SOC_evess		=	optimvar('SOC_evess',length(Input.simulation.Samples),Input.simulation.Entities,'LowerBound',repmat(Input.evess.SOC_min,length(Input.simulation.Samples),1),'UpperBound',repmat(Input.evess.SOC_max,length(Input.simulation.Samples),1));

	CRate_sess		=	optimvar('CRate_sess',length(Input.simulation.Samples),Input.simulation.Entities,'LowerBound',repmat(-Input.sess.CRate_out,length(Input.simulation.Samples),1),'UpperBound',repmat(Input.sess.CRate_in,length(Input.simulation.Samples),1));
	CRate_evess		=	optimvar('CRate_evess',length(Input.simulation.Samples),Input.simulation.Entities,'LowerBound',repmat(-Input.evess.CRate_out,length(Input.simulation.Samples),1),'UpperBound',repmat(Input.evess.CRate_in,length(Input.simulation.Samples),1));

	EFC_sess		=	optimvar('EFC_sess',length(Input.simulation.Samples),Input.simulation.Entities,'LowerBound',0,'UpperBound',Inf);
	EFC_evess		=	optimvar('EFC_evess',length(Input.simulation.Samples),Input.simulation.Entities,'LowerBound',0,'UpperBound',Inf);

%% Define Objective function
%Maximize the profit from energy trading
if Input.simulation.SimpleCharge == double(false)
	OptProb = optimproblem('ObjectiveSense','maximize','Objective',sum(Input.grid.SellingPrice.*P_ac_pccTgrid*Input.simulation.SampleTime-Input.grid.PurchasingPrice.*P_ac_pccFgrid*Input.simulation.SampleTime));
else
	OptProb = optimproblem('ObjectiveSense','maximize','Objective',(sum(Input.grid.SellingPrice.*P_ac_pccTgrid*Input.simulation.SampleTime-Input.grid.PurchasingPrice.*P_ac_pccFgrid*Input.simulation.SampleTime))+sum(SOC_evess));
end

%% Define Constraints
	C101 =	0 == P_acbbFdcbb+P_ac_bbFpcc-P_acbbTdcbb-P_ac_bbTpcc-Input.load.PowerLoad+Input.simulation.ModuleGeneration*P_ac_bbFgen.*repmat(Input.simulation.AC_Generation,length(Input.simulation.Samples),1)+Input.simulation.ModuleSESS*(P_ac_bbFsess-P_ac_bbTsess)+Input.simulation.ModuleEVESS*(P_ac_bbFevess.*Input.chargingstation.PluggedEV.*repmat(Input.simulation.V2B,length(Input.simulation.Samples),1)-P_ac_bbTevess.*Input.chargingstation.PluggedEV);
	C102 =	0 == P_dcbbFacbb-P_dcbbTacbb+Input.simulation.ModuleGeneration*(P_dc_bbFgen.*repmat(Input.simulation.DC_Generation,length(Input.simulation.Samples),1))+Input.simulation.ModuleSESS*(P_dc_bbFsess-P_dc_bbTsess)+Input.simulation.ModuleEVESS*(P_dc_bbFevess.*Input.chargingstation.PluggedEV.*repmat(Input.simulation.V2B,length(Input.simulation.Samples),1)-P_dc_bbTevess.*Input.chargingstation.PluggedEV);
	C111 =	P_acbbTdcbb.*repmat(Input.busbar.Eta_ACDC,length(Input.simulation.Samples),1) == P_dcbbFacbb;
	C112 =	P_dcbbTacbb.*repmat(Input.busbar.Eta_ACDC,length(Input.simulation.Samples),1) == P_acbbFdcbb;

if Input.simulation.EnergySharing == true
	C301 =	sum(P_ac_pccFbb,2)+P_ac_pccFgrid == sum(P_ac_pccTbb,2)+P_ac_pccTgrid;
	C302 =	C301;
	C303 =	P_ac_pccTgrid(:)-P_ac_pccFgrid(:) <= sum(Input.generation.PeakPower.*Input.grid.Curtailment_Limit);
else
	C301 =	sum(P_ac_pccFbb,2) == P_ac_pccTgrid(:);
	C302 =	sum(P_ac_pccTbb,2) == P_ac_pccFgrid(:);
	C303 =	P_ac_pccFbb-P_ac_pccTbb <= Input.generation.PeakPower.*repmat(Input.grid.Curtailment_Limit,length(Input.simulation.Samples),1);
end
	C311 =	P_ac_bbTpcc == P_ac_pccFbb;
	C312 =	P_ac_pccTbb == P_ac_bbFpcc;

if Input.simulation.ModuleGeneration == double(true)
	C501 =	Input.generation.PowerGeneration == P_curtailment+P_ac_genTbb.*repmat(Input.simulation.AC_Generation,length(Input.simulation.Samples),1)+P_dc_genTbb.*repmat(Input.simulation.DC_Generation,length(Input.simulation.Samples),1);
	C511 =	P_ac_genTbb.*repmat(Input.generation.Eta_ACDC,length(Input.simulation.Samples),1) == P_ac_bbFgen;
	C512 =	P_dc_genTbb.*repmat(Input.generation.Eta_DCDC,length(Input.simulation.Samples),1) == P_dc_bbFgen;
end

if Input.simulation.ModuleSESS == double(true)
	C601 =	P_dc_bbTsess.*repmat(Input.sess.Eta_DCDC,length(Input.simulation.Samples),1) == P_dc_sessFbb;
	C602 =	P_dc_sessTbb.*repmat(Input.sess.Eta_DCDC,length(Input.simulation.Samples),1) == P_dc_bbFsess;
	C603 =	P_ac_bbTsess.*repmat(Input.sess.Eta_ACDC,length(Input.simulation.Samples),1) == P_ac_sessFbb;
	C604 =	P_ac_sessTbb.*repmat(Input.sess.Eta_ACDC,length(Input.simulation.Samples),1) == P_ac_bbFsess;
	C621 =	E_sess_in == (P_ac_sessFbb+P_dc_sessFbb)*Input.simulation.SampleTime;
	C622 =	E_sess_out == (P_ac_sessTbb+P_dc_sessTbb)*Input.simulation.SampleTime;
	C631 =	SOC_sess == E_sess./Input.sess.EnergyAvailable;
	C641 =	CRate_sess == ((60*60)/Input.simulation.SampleTime)*((E_sess_in-E_sess_out)./repmat(Input.sess.EnergyNominal,length(Input.simulation.Samples),1));
	C642 =	EFC_sess == 0.5*(E_sess_in+E_sess_out)./repmat(Input.sess.EnergyNominal,length(Input.simulation.Samples),1);
	C651 =	sparse(diag(ones(length(Input.simulation.Samples)-1,1),-1))*E_sess+E_sess_in.*repmat(Input.sess.Eta_EES_in,length(Input.simulation.Samples),1) == E_sess+E_sess_out.*repmat((1./Input.sess.Eta_EES_out),length(Input.simulation.Samples),1)+Input.sess.EnergySelfDischarge;
	C651(1,:) =	Input.sess.EnergyAvailable(1,:).*Input.sess.SOC_initial+E_sess_in(1,:).*Input.sess.Eta_EES_in == E_sess(1,:)+E_sess_out(1,:).*(1./Input.sess.Eta_EES_out)+Input.sess.EnergySelfDischarge(1,:);
	C661 =	E_sess(1,:) == Input.sess.EnergyAvailable(1,:).*Input.sess.SOC_initial;
	C662 =	E_sess(length(Input.simulation.Samples),:) == Input.sess.EnergyAvailable(length(Input.simulation.Samples),:).*Input.sess.SOC_end;
end

if Input.simulation.ModuleEVESS == double(true)
	C701 =	P_dc_bbTevess.*repmat(Input.chargingstation.Eta_DCDC,length(Input.simulation.Samples),1) == P_dc_evessFbb;
	C702 =	P_dc_evessTbb.*repmat(Input.chargingstation.Eta_DCDC,length(Input.simulation.Samples),1) == P_dc_bbFevess;
	C703 =	P_ac_bbTevess.*repmat(Input.chargingstation.Eta_ACDC,length(Input.simulation.Samples),1) == P_ac_evessFbb;
	C705 =	P_ac_evessTbb.*repmat(Input.chargingstation.Eta_ACDC,length(Input.simulation.Samples),1) == P_ac_bbFevess;
	C706 =	Input.ev.PowerEVRecuperation.*repmat(Input.evess.Eta_ACDC,length(Input.simulation.Samples),1) == P_evessFevdrive;
	C707 =	P_evessTevdrive.*repmat(Input.evess.Eta_ACDC,length(Input.simulation.Samples),1) == Input.ev.PowerEVLoad;
	C721 =	E_evess_in == ((P_ac_evessFbb+P_dc_evessFbb).*Input.chargingstation.PluggedEV+P_evessFevdrive)*Input.simulation.SampleTime;
	C722 =	E_evess_out == ((P_ac_evessTbb+P_dc_evessTbb).*Input.chargingstation.PluggedEV.*repmat(Input.simulation.V2B,length(Input.simulation.Samples),1)+P_evessTevdrive)*Input.simulation.SampleTime;
	C731 =	SOC_evess == E_evess./Input.evess.EnergyAvailable;
	C741 =	CRate_evess == ((60*60)/Input.simulation.SampleTime)*((E_evess_in-E_evess_out)./repmat(Input.evess.EnergyNominal,length(Input.simulation.Samples),1));
	C742 =	EFC_evess == 0.5*(E_evess_in+E_evess_out)./repmat(Input.evess.EnergyNominal,length(Input.simulation.Samples),1);
	C751 =	sparse(diag(ones(length(Input.simulation.Samples)-1,1),-1))*E_evess+E_evess_in.*repmat(Input.evess.Eta_EES_in,length(Input.simulation.Samples),1) == E_evess+E_evess_out.*repmat((1./Input.evess.Eta_EES_out),length(Input.simulation.Samples),1)+Input.evess.EnergySelfDischarge;
	C751(1,:) =	Input.evess.EnergyAvailable(1,:).*Input.evess.SOC_initial+E_evess_in(1,:).*Input.evess.Eta_EES_in == E_evess(1,:)+E_evess_out(1,:).*(1./Input.evess.Eta_EES_out)+Input.evess.EnergySelfDischarge(1,:);
	C761 =	E_evess(1,:) == Input.evess.EnergyNominal(1,:).*Input.evess.SOC_initial;
	C762 =	E_evess(length(Input.simulation.Samples),:) == Input.evess.EnergyAvailable(length(Input.simulation.Samples),:).*Input.evess.SOC_end;
end

%% Call constraints
	OptProb.Constraints.C101 =	C101;
	OptProb.Constraints.C102 =	C102;
	OptProb.Constraints.C111 =	C111;
	OptProb.Constraints.C112 =	C112;

	OptProb.Constraints.C301 =	C301;
	OptProb.Constraints.C302 =	C302;
	OptProb.Constraints.C303 =	C303;
	OptProb.Constraints.C311 =	C311;
	OptProb.Constraints.C312 =	C312;

if Input.simulation.ModuleGeneration == double(true)
	OptProb.Constraints.C501 =	C501;
	OptProb.Constraints.C511 =	C511;
	OptProb.Constraints.C512 =	C512;
end

if Input.simulation.ModuleSESS == double(true)
	OptProb.Constraints.C601 =	C601;
	OptProb.Constraints.C602 =	C602;
	OptProb.Constraints.C603 =	C603;
	OptProb.Constraints.C604 =	C604;
	OptProb.Constraints.C621 =	C621;
	OptProb.Constraints.C622 =	C622;
	OptProb.Constraints.C631 =	C631;
	OptProb.Constraints.C641 =	C641;
	OptProb.Constraints.C642 =	C642;
	OptProb.Constraints.C651 =	C651;
	OptProb.Constraints.C661 =	C661;
	OptProb.Constraints.C662 =	C662;
end

if Input.simulation.ModuleEVESS == double(true)
	OptProb.Constraints.C701 =	C701;
	OptProb.Constraints.C702 =	C702;
	OptProb.Constraints.C703 =	C703;
	OptProb.Constraints.C705 =	C705;
	OptProb.Constraints.C706 =	C706;
	OptProb.Constraints.C707 =	C707;
	OptProb.Constraints.C721 =	C721;
	OptProb.Constraints.C722 =	C722;
	OptProb.Constraints.C731 =	C731;
	OptProb.Constraints.C741 =	C741;
	OptProb.Constraints.C742 =	C742;
	OptProb.Constraints.C751 =	C751;
	OptProb.Constraints.C761 =	C761;
	OptProb.Constraints.C762 =	C762;
end