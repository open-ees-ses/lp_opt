%% Script Description
% 	Coded and provided by:
% 	Stefan Englberger (stefan.englberger@tum.de)
% 	Team Stationary Energy Storage Systems (SES)
% 	Institute for Electrical Energy Storage Technology
% 	TUM Department of Electrical and Computer Engineering
% 	Technical University of Munich
%
% 2019-07-11 Stefan Englberger (author)
%
% Englberger, S., Hesse, H., Kucevic, D., & Jossen, A. (2019). A Techno-Economic Analysis 
% of Vehicle-to-Building: % Battery Degradation and Efficiency Analysis in the Context of 
% Coordinated Electric Vehicle Charging. Energies, 12(5), 955.

close all; clear; clc;

%% PreProcessing
script_01_pre_processing;

	%% Parameter
	disp('<strong>Read parameter.</strong>'); tic;
	if exist('Input.xlsx', 'file') == 2
		script_11_call_parameter;
	else
		script_12_create_parameter;
	end
	disp(['   Elap sed time: ',sprintf('%0.2f',toc),' sec (',datestr(now,'yyyy-mm-dd HH:MM:SS'),')']);

	%% PreCalculation
	disp('<strong>Run pre-calculation.</strong>'); tic;
	script_13_pre_calculation;
	disp(['   Elapsed time: ',sprintf('%0.2f',toc),' sec (',datestr(now,'yyyy-mm-dd HH:MM:SS'),')']);

	%% CallProfile
	disp('<strong>Call profiles.</strong>'); tic;
	script_14_call_profile;
	disp(['   Elapsed time: ',sprintf('%0.2f',toc),' sec (',datestr(now,'yyyy-mm-dd HH:MM:SS'),')']);
	clearvars -except Input;

	%% ProblemDefinition
	disp('<strong>Define optimization problem.</strong>'); tic;
	script_20_problem_definition;
	disp(['   Elapsed time: ',sprintf('%0.2f',toc),' sec (',datestr(now,'yyyy-mm-dd HH:MM:SS'),')']);

	%% ProblemSolver
	disp('<strong>Solve optimization problem.</strong>'); tic;
	script_21_problem_solver;
	disp(['   Elapsed time: ',sprintf('%0.2f',toc),' sec (',datestr(now,'yyyy-mm-dd HH:MM:SS'),')']);

	%% Plotting
	disp('<strong>Plot results.</strong>'); tic;
	script_31_plotting;
	disp(['   Elapsed time: ',sprintf('%0.2f',toc),' sec (',datestr(now,'yyyy-mm-dd HH:MM:SS'),')']);

%% PostProcessing
script_32_post_processing;